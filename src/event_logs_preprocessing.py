import pandas as pd
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
import numpy as np


def label_encoding(df, col_names):
    le = LabelEncoder()
    for col_name in col_names:
        df[col_name] = le.fit_transform(df[col_name].values)
    return df


def one_hot_encoding(df, col_names):
    df = pd.get_dummies(df, columns=col_names, dtype=float, sparse=True)
    return df


def scale_data(df, col_names):
    min_max_scaler = MinMaxScaler()
    df[[f"{column}" for column in col_names]] = min_max_scaler.fit_transform(df[[f"{column}" for column in col_names]])


df = pd.read_csv('data/unprocessed/all_events.csv')
df.drop('BlockId', axis=1, inplace=True)
df = label_encoding(df, ['Label', 'EventSequence'])
#scale_data(df, ['EventSequence'])
df['Label'] = np.logical_xor(df['Label'], 1).astype(int)
df.to_csv('data/processed/processed.csv', index=False)