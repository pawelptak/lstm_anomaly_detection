import pandas as pd
pd.options.plotting.backend = "plotly"
from keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from keras import regularizers
from keras.models import Model
from keras.layers import LSTM, Dense, Input, RepeatVector, TimeDistributed
from keras.callbacks import EarlyStopping
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix


def autoencoder_model(X):
    num_sequences = X.shape[1]
    num_features = X.shape[2]
    inputs = Input(shape=(num_sequences, num_features))
    L1 = LSTM(16, activation='relu', return_sequences=True,
              kernel_regularizer=regularizers.l2(0.00))(inputs)
    L2 = LSTM(4, activation='relu', return_sequences=False)(L1)
    L3 = RepeatVector(X.shape[1])(L2)
    L4 = LSTM(4, activation='relu', return_sequences=True)(L3)
    L5 = LSTM(16, activation='relu', return_sequences=True)(L4)
    output = TimeDistributed(Dense(X.shape[2]))(L5)
    model = Model(inputs=inputs, outputs=output)
    return model


def fit_model(model, X_train, y_train, epochs, batch_size, val_split, plot=True):
    callback = EarlyStopping(monitor='loss', patience=10)
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,
                        validation_split=val_split, shuffle=False, callbacks=[callback]).history

    if plot:
        # plot the training losses
        fig, ax = plt.subplots(figsize=(14, 6), dpi=80)
        ax.plot(history['loss'], 'b', label='Train', linewidth=2)
        ax.plot(history['val_loss'], 'r', label='Validation', linewidth=2)
        ax.set_title('Model loss', fontsize=16)
        ax.set_ylabel('Loss (mae)')
        ax.set_xlabel('Epoch')
        ax.legend(loc='upper right')
        plt.show()

    model.save('lstm_model.h5')


def generate_class_plot(df, col_name):
    column = df[col_name]
    classes_list = column.unique()
    print(classes_list)
    y = []
    for c in classes_list:
        y.append(column.value_counts()[c])

    n = len(df.index)
    k = len(df)

    for i in range(len(classes_list)):  # show values on bars
        plt.text(classes_list[i], y[i] + 1, y[i])

    plt.bar(classes_list, y)
    plt.xticks(classes_list)
    plt.xlabel(col_name)
    plt.ylabel("Count")
    plt.show()

if __name__ == "__main__":
    all_data = pd.read_csv('data/processed/processed.csv')

    train_percent = .9  # train data percentage
    normal_logs = all_data[all_data['Label'] == 0]
    anomaly_logs = all_data[all_data['Label'] != 0]

    train_size = int(train_percent * len(normal_logs))

    train_data = normal_logs[:train_size]
    test_data = pd.concat([normal_logs[train_size:], anomaly_logs])

    generate_class_plot(train_data, 'Label')
    generate_class_plot(test_data, 'Label')

    train_data_labels = train_data.pop('Label')  # remove label from data
    test_data_labels = test_data.pop('Label')  # remove label from data

    print(f"Train data size: {len(train_data)}. Test data size: {len(test_data)}")

    train = np.array(train_data)

    test = np.array(test_data)

    # reshape inputs for LSTM [samples, timesteps, features]
    X_train = train.reshape((train.shape[0], 1, train.shape[1]))
    X_test = test.reshape((test.shape[0], 1, test.shape[1]))

    model = autoencoder_model(X_train)
    model.compile(optimizer='adam', loss='mae')
    model.summary()

    # fit the model to the data
    fit_model(model, X_train, X_train, epochs=300, batch_size=32, val_split=0.05)

    model = load_model('lstm_model.h5')

    predicted = model.predict(X_train)
    train_mae_loss = np.mean(np.abs(predicted - X_train), axis=tuple(range(1, X_train.ndim)))
    train_score_df = pd.DataFrame()
    train_score_df['loss'] = train_mae_loss
    plt.title('Loss Distribution', fontsize=16)
    sns.distplot(train_score_df['loss'], kde=True)
    plt.show()

    threshold = max(train_mae_loss)
    #threshold = .0011

    predicted = model.predict(X_test)
    test_mae_loss = np.mean(np.abs(predicted - X_test), axis=tuple(range(1, X_test.ndim)))
    test_score_df = test_data.copy()
    test_score_df['loss'] = test_mae_loss
    test_score_df['threshold'] = threshold
    test_score_df['anomaly'] = (test_score_df['loss'] > test_score_df['threshold']) * 1
    test_score_df['actual value'] = test_data_labels

    # with pd.option_context('display.max_rows', None):  # more options can be specified also
    #     print(test_score_df[['anomaly', 'actual value', 'loss', 'threshold']])

    plot_df = test_score_df.reset_index()[['loss', 'threshold']]
    colors = ['r' if flag == 1 else 'b' for flag in test_score_df['actual value']]
    fig = plot_df.plot.scatter(color=colors, x=plot_df.index, y=['loss', 'threshold'])
    fig.show()
    # fig = plot_df.plot(color=colors)
    # fig.show()

    conf_matrix = confusion_matrix(test_score_df['actual value'], test_score_df['anomaly'])
    sns.set(rc={'figure.figsize':(10, 8)})
    ax = sns.heatmap(conf_matrix, annot=True, cmap='Blues', fmt='g')
    ax.set_xlabel('\nPredicted Values')
    ax.set_ylabel('Actual Values ')

    ## Ticket labels - List must be in alphabetical order
    ax.xaxis.set_ticklabels(['No anomaly', 'Anomaly'])
    ax.yaxis.set_ticklabels(['No anomaly', 'Anomaly'])

    plt.title(f"Train: {len(train_data)}. Test: {len(test_data)}. Threshold: {threshold}")

    plt.show()